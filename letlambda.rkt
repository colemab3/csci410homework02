#lang plai-typed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;CSCI 410 Programming languages
;;Professor Matthews
;;Fall 2014
;;
;;Brody Coleman
;;W00905954
;;
;;Homework 2, letlambda.rkt
;;
;;
;; Started with rkt plai2-07-03, modified it to 
;; conform with the homework requirements.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-type ExprC
   [numC (n : number)]
   [idC (s : symbol)]
   [appC (fun : ExprC) (arg : ExprC)]
   [plusC (l : ExprC) (r : ExprC)]
   [multC (l : ExprC) (r : ExprC)]
   [eqC (l : ExprC)(r : ExprC)]
   [ifC (if : ExprC)(then : ExprC)(else : ExprC)]
   [lamC (arg : symbol) (body : ExprC)])

(define-type ExprS
   [numS (n : number)]
   [idS (s : symbol)]
   [appS (fun : ExprS) (arg : ExprS)]
   [plusS (l : ExprS) (r : ExprS)]
   [multS (l : ExprS) (r : ExprS)]
   [ifS (if : ExprS)(then : ExprS)(else : ExprS)]
   [eqS (l : ExprS)(r : ExprS)]
   [lamS (arg : symbol) (body : ExprS)]
   [letS (a : symbol)(b : ExprS)(c : ExprS)])

(define-type Binding
   [bind (name : symbol) (val : Value)])
 
 (define-type-alias Env (listof Binding))
 (define mt-env empty)
 (define extend-env cons)

(define-type Value
   [numV (n : number)]
   [closV (arg : symbol) (body : ExprC) (env : Env)])

(define (parse [s : s-expression]) : ExprS
  (cond
    [(s-exp-number? s)
     (numS (s-exp->number s))]
    [(s-exp-symbol? s)
     (idS (s-exp->symbol s))]
    [(s-exp-list? s)
     (let ([sl (s-exp->list s)])
       (if (s-exp-symbol? (first sl))
           (case (s-exp->symbol (first sl))
             [(+)(plusS (parse (second sl)) (parse (third sl)))]
             [(*)(multS (parse (second sl)) (parse (third sl)))]
             [(=)(eqS (parse (second sl)) (parse (third sl)))]
             [(lambda) (lamS (s-exp->symbol (first (s-exp->list (second sl))))
                             (parse (third sl)))]
             [(if)(ifS (parse (second sl)) (parse (third sl))(parse (fourth sl)))]
             [(let)(letS (s-exp->symbol (first(s-exp->list(first(s-exp->list (second sl))))))
                         (parse (second(s-exp->list(first(s-exp->list (second sl))))))(parse (third sl)))]
             [else
              (appS (parse (first sl))
                    (parse (second sl))) ]
             )
           (appS (parse (first sl))
                 (parse (second sl)))))]      
    [else (error 'parse "invalid input")]))

(define (interp [expr : ExprC] [env : Env]) : Value
   (type-case ExprC expr
     [numC (n) (numV n)]
     [idC (n) (lookup n env)]
     [plusC (l r) (num+ (interp l env) (interp r env))]
     [multC (l r) (num* (interp l env) (interp r env))]
     [lamC (a b) (closV a b env)]
     [ifC (i t e) (cond[(eq? (numV-n(interp i env))(numV-n(numV 0)))(interp e env)]
                      [else (interp t env)])]
     [eqC (r l)(cond[(eq? (interp r env)(interp l env)) (numV 1)]
                    [else (numV 0)])]
     [appC (f a) (local ([define f-value (interp f env)])
               (interp (closV-body f-value)
                       (extend-env (bind (closV-arg f-value)
                                         (interp a env))
                                   (closV-env f-value))))]))

(define (desugar [expr : ExprS]) : ExprC
  (type-case ExprS expr
   [numS (n) (numC n)]
   [idS (s) (idC s)]
   [appS (fun arg)(appC (desugar fun) (desugar arg))]
   [plusS (l r)(plusC (desugar l) (desugar r))]
   [multS (l r)(multC (desugar l) (desugar r))]
   [lamS (arg body)(lamC arg (desugar body))]
   [eqS (l r)(eqC (desugar l) (desugar r))]
   [ifS (i t e)(ifC (desugar i) (desugar t) (desugar e))]
   [letS (a b c)(appC (lamC a (desugar c))(desugar b))]))

(define (lookup (n : symbol) (env : Env)) : Value
   (cond ((empty? env) (error 'lookup "name not found"))
         ((symbol=? n (bind-name (first env)))
          (bind-val (first env)))
         (else (lookup n (rest env)))))

(define (num+ [l : Value] [r : Value]) : Value
   (cond
     [(and (numV? l) (numV? r))
      (numV (+ (numV-n l) (numV-n r)))]
     [else
      (error 'num+ "one argument was not a number")]))

(define (num* [l : Value] [r : Value]) : Value
   (cond
     [(and (numV? l) (numV? r))
      (numV (* (numV-n l) (numV-n r)))]
     [else
      (error 'num* "one argument was not a number")]))


;; *** all my testing ***
;;(interp(desugar(parse '(let((f 6)) f)))mt-env)
;;(define g (numC 0))
;;(interp g mt-env)
;;(numV-n (interp g mt-env))
;;(parse '(if 0 (+ 0 0) 10))
;;(desugar(parse '(if 1 (+ 0 0) 10)))
;;(interp (desugar(parse '(if 1 (+ 0 0) 10))) mt-env)
;;(eq? (numV-n (interp g mt-env)) (numV-n (interp g mt-env)))
;;(interp(desugar(parse '(if g 5 6)))mt-env)
;;(interp(desugar(parse '(= 0 5)))mt-env)
;;(interp(desugar(parse '(+ 0 1)))mt-env)
;;(test (parse '((lambda (x) (+ x x)) 3))
;;      (appS (lamS 'x (plusS (idS 'x) (idS 'x))) (numS 3)))
;;(parse '(let ((f (lambda (x) (if x (+ x x) 10))))
;;(+ (f 0) (f 3))))
;;(desugar(parse '(let ((f (lambda (x) (if x (+ x x) 10))))
;;(+ (f 0) (f 3)))))

(test (interp (desugar(parse '((lambda (x) (+ x x)) 3))) mt-env)
     (numV 6))

;;*example test case from the Homework problem statement. *
;;(parse '(let ((f (lambda (x) (if x (+ x x) 10))))(+ (f 0) (f 3))))
;;(desugar(parse '(let ((f (lambda (x) (if x (+ x x) 10))))(+(f 0)(f 3)))))
(test(interp(desugar(parse '(let ((f (lambda (x) (if x (+ x x) 10))))(+(f 0)(f 3)))))mt-env)(numV 16))